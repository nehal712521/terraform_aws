resource "aws_vpc" "myvpc" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true
  tags = {
    Name = "myvpc"
  }
}

resource "aws_subnet" "pn_sn" {
  vpc_id = aws_vpc.myvpc.id
  cidr_block = "10.0.0.0/24"
  map_public_ip_on_launch = true
  availability_zone = "ap-south-1a"

  tags = {
    Name = "pn_sn1"
  }
  
}

resource "aws_security_group" "sg" {
  vpc_id = aws_vpc.myvpc.id
  name = "my_sg"
  description = "public security"

  ingress   {
    cidr_blocks = [ "0.0.0.0/0" ]
    description = "allow ssh"
    from_port = 22
    to_port = 22
    protocol = "tcp"
   
  }   

  egress   {
    cidr_blocks = [ "0.0.0.0/0" ]
    description = "value"
    from_port = 0
    protocol = "-1"
    to_port = 0
  } 
  
}