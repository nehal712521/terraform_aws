module "vpc" {
  source = "./vpc"  
}

module "ec2" {
  source = "./web"
  sn = module.vpc.pn_sn
  sg = module.vpc.sg
}