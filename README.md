# AWS EC2 Instance Automation with Terraform

This repository contains automation scripts for launching an EC2 instance on AWS using Terraform. The automation is divided into multiple stages, providing a seamless deployment process.

## Prerequisites

Before running the scripts, ensure that you have the following prerequisites installed:

- [Terraform](https://www.terraform.io/) - Infrastructure as Code tool for building, changing, and versioning infrastructure
- AWS Account - Access key and secret key with appropriate permissions configured locally

## Getting Started

Follow these steps to automate the launch of an EC2 instance on AWS:

1. Clone the repository to your local machine:

    ```bash
    git clone https://gitlab.com/nehal712521/terraform_aws.git
    ```

2. Navigate to the repository directory:

    ```bash
    cd aws-ec2-automation
    ```

3. Update the `terraform.tfvars` file with your AWS access key, secret key, and desired instance configurations.

4. Run the following Terraform commands to initialize and apply the configuration:

    ```bash
    terraform init
    terraform apply
    ```

5. Confirm the execution plan and apply the changes.

6. Once the instance is launched, access the AWS Management Console to view the newly created EC2 instance.

## Manual Steps

In this automation process, the deployment is divided into stages. The third stage involves running the Terraform plan, and the fourth stage involves manually triggering the Terraform destroy command. Follow these manual steps to complete the process:

1. **Stage 3: Terraform Plan:**

    After applying the Terraform configuration, you can run the Terraform plan command to review the proposed changes without applying them:

    ```bash
    terraform plan
    ```

    Review the plan to ensure it aligns with your expectations.

2. **Stage 4: Terraform Destroy:**

    To destroy the resources created by Terraform, run the following command:

    ```bash
    terraform destroy
    ```

    Confirm the destruction of resources when prompted.

## Additional Notes

- Ensure that you have proper AWS credentials and permissions configured to avoid any authentication issues.
- Review the Terraform configuration files (`main.tf`, `variables.tf`, etc.) to customize the deployment according to your requirements.
- For any questions or issues, please refer to the [Terraform documentation](https://www.terraform.io/docs/index.html) or reach out to the repository owner.

Happy automating!